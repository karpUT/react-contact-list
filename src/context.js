import React, { Component } from "react";

const Context = React.createContext();

const reducer = (state, action) => {
  switch (action.type) {
    case "DELETE_CONTACT":
      return {
        ...state,
        contacts: state.contacts.filter(contact => contact.id !== action.payload)
      };
  }
};

export class Provider extends Component {
  state = {
    contacts: [
      {
        id: 1,
        name: "Kristo Karp",
        email: "KristoKarp@gmail.com",
        phone: "55669161"
      },
      {
        id: 2,
        name: "Kristo Karp1",
        email: "KristoKarp@gmail.com",
        phone: "55669162"
      },
      {
        id: 3,
        name: "Kristo Karp2",
        email: "KristoKarp@gmail.com",
        phone: "55669163"
      }
    ],
    dispatch: action => this.setState(state => reducer(state, action))
  };

  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

export const Consumer = Context.Consumer;
